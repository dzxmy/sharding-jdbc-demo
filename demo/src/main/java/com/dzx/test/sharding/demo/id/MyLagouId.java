package com.dzx.test.sharding.demo.id;

import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;

import java.util.Properties;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月13日 16:51:48
 */
public class MyLagouId implements ShardingKeyGenerator {

    private static final SnowflakeShardingKeyGenerator SNOW = new SnowflakeShardingKeyGenerator();

    @Override
    public Comparable<?> generateKey() {
        System.out.println("--------执行了自定义的主键生成器----------");
        return SNOW.generateKey();
    }

    @Override
    public String getType() {
        return "LAGOUKEY";
    }

    @Override
    public Properties getProperties() {
        return null;
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
