package com.dzx.test.sharding.demo.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月15日 19:29:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMsg<T> {

    private static final int SUCCESS = 200;

    private static final int FAIL = 500;

    private int code;

    private T t;

    private String msg;

    public ResponseMsg(int code, T t) {
        this.code = code;
        this.t = t;
    }

    public static <T> ResponseMsg buildSuccessMsg(T t) {
        return new <T>ResponseMsg(SUCCESS, t, "SUCCESS");
    }

    public static <T> ResponseMsg buildFail(T t) {
        return new ResponseMsg(FAIL, t, "FAIL");
    }

}
