package com.dzx.test.sharding.demo.dao;

import com.dzx.test.sharding.demo.entity.PositionDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月13日 20:11:42
 */
public interface PositionDetailRepository extends JpaRepository<PositionDetail,Long> {

}
