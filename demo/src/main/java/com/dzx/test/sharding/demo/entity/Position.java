package com.dzx.test.sharding.demo.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月13日 14:39:33
 */
@Entity
@Table(name = "position")
@Data
public class Position {

    /**
     * `Id` BIGINT(11) NOT null auto_increment,
     * `name` VARCHAR(256) DEFAULT null,
     * `salary` VARCHAR(50) DEFAULT null,
     * `city` VARCHAR(256) DEFAULT null,
     */

    @Id
    @Column(name = "id")
    //自动生成id策略
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "salary")
    private String salary;

    @Column(name = "city")
    private String city;

}
