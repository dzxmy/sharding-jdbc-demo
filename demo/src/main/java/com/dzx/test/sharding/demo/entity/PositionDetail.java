package com.dzx.test.sharding.demo.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月13日 18:24:44
 */
@Entity
@Table(name = "position_detail")
@Data
public class PositionDetail implements Serializable {

    /**
     *   `pid` bigint(11) NOT NULL DEFAULT '0',
     *   `description` text,
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  long id;

    @Column(name = "pid")
    private long pid;

    @Column(name = "description")
    private String description;
}
