package com.dzx.test.sharding.demo.dao;

import com.dzx.test.sharding.demo.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月13日 14:44:24
 */
public interface PositionRepository extends JpaRepository<Position, Long> {

    @Query(nativeQuery = true,value = "select p.id,p.name,p.salary,p.city,pd.description from position p inner join " +
            "position_detail pd on (p.id=pd.pid) where p.id=:id")
    public Object findByPositionId(@Param("id") long id);

}
