package com.dzx.test.sharding.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月15日 19:04:28
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "boots.module.minio")
public class MinioData {
    /**
     * minio地址+端口号
     */
    private String url;

    /**
     * minio用户名
     */
    private String accessKey;

    /**
     * minio密码
     */
    private String secretKey;

    /**
     * 文件桶的名称
     */
    private String bucketName;
}
