package com.dzx.test.sharding.demo.dao;

import com.dzx.test.sharding.demo.entity.BOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月15日 09:39:09
 */
public interface BOrderRepository extends JpaRepository<BOrder,Long> {

}
