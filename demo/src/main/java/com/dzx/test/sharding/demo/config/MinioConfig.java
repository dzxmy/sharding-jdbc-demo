package com.dzx.test.sharding.demo.config;

import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月15日 19:05:53
 */
@Slf4j
@Configuration
public class MinioConfig {

    @Autowired
    private MinioData minioData;

    @Bean
    public MinioClient minioClient() throws Exception {
        try {
            return new MinioClient(minioData.getUrl(), minioData.getAccessKey(), minioData.getSecretKey());
        } catch (final Exception e) {
            log.error("初始化minno出现异常{}", e.fillInStackTrace());
            throw new Exception(e.getMessage());
        }
    }


}
