package com.dzx.test.sharding.demo;

import com.dzx.test.sharding.demo.dao.BOrderRepository;
import com.dzx.test.sharding.demo.dao.PositionDetailRepository;
import com.dzx.test.sharding.demo.dao.PositionRepository;
import com.dzx.test.sharding.demo.entity.BOrder;
import com.dzx.test.sharding.demo.entity.Position;
import com.dzx.test.sharding.demo.entity.PositionDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.security.SecureRandom;
import java.util.Date;

@SpringBootApplication
@Slf4j
public class DemoApplication implements CommandLineRunner {

    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private PositionDetailRepository positionDetailRepository;

    @Autowired
    private BOrderRepository bOrderRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        /**
         * 测试1
         */
        for (int i = 1; i <= 20; i++) {
            Position position = new Position();
//            position.setId((long) i);
            position.setName("lagou" + i);
            position.setSalary("1000000");
            position.setCity("beijing");
            positionRepository.save(position);

            PositionDetail positionDetail = new PositionDetail();
            positionDetail.setPid(position.getId());
            positionDetail.setDescription("this is a message" + i);
            positionDetailRepository.save(positionDetail);
        }

        /**
         * 查询lagou1 数据库中的一条记录
         */
        Object object = positionRepository.findByPositionId(500765944646205440L);
        log.info(object.toString());
        Object[] objects = (Object[]) object;
        log.info("id:{},name:{},salary:{},city:{},description:{}", objects[0]
                , objects[1], objects[2], objects[3], objects[4]);

        /**
         * 查询lagou2 数据库中的一条记录
         */
        Object object2 = positionRepository.findByPositionId(500765944360992769L);
        log.info(object2.toString());
        Object[] objects2 = (Object[]) object2;
        log.info("id:{},name:{},salary:{},city:{},description:{}", objects2[0]
                , objects2[1], objects2[2], objects2[3], objects2[4]);

        /**
         * 排序分页查询
         */
        PageRequest pageRequest = PageRequest.of(1, 20, Sort.by(Sort.Order.desc("id")));
        Page<Position> all = positionRepository.findAll(pageRequest);
        all.get().forEach(System.out::println);


        for (int i = 0; i < 100; i++) {
            BOrder order = new BOrder();
            SecureRandom secureRandom = new SecureRandom();
            int companyId = secureRandom.nextInt(10);
            order.setIsDel(false);
            order.setCompanyId(companyId);
            order.setPositionId(332456);
            order.setUserId(2222);
            order.setPublishUserId(1111);
            order.setResumeType(1);
            order.setStatus("AUTO");
            order.setCreateTime(new Date());
            order.setOperateTime(new Date());
            order.setWorkYear("2");
            order.setName("lagou");
            order.setPositionName("java");
            order.setResumeId(23233);
            bOrderRepository.save(order);
        }
    }
}
