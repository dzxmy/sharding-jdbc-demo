package com.dzx.test.sharding.demo.controller;

import com.dzx.test.sharding.demo.config.MinioData;
import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.PutObjectOptions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;

/**
 * @author 80004819
 * @ClassName:
 * @Description:
 * @date 2020年08月15日 19:12:06
 */
@SuppressWarnings("deprecation")
@Api(tags = {"web服务：minio上传,下载,删除接口"})
@RestController
@RequestMapping("/view/minio")
public class MinioView {
    @Autowired
    private MinioClient minioClient;

    @Autowired
    private MinioData minioData;

    @ApiOperation(value = "下载文件")
    @GetMapping(value = "/download")
    @SneakyThrows(Exception.class)
    public void download(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        InputStream inputStream = null;
        final ObjectStat stat = minioClient.statObject(minioData.getBucketName(), fileName);
        response.setContentType(stat.contentType());
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        inputStream = minioClient.getObject(minioData.getBucketName(), fileName);
        IOUtils.copy(inputStream, response.getOutputStream());
        inputStream.close();
    }

    @ApiOperation(value = "上传文件")
    @PostMapping(value = "/upload")
    @SneakyThrows(Exception.class)
    public ResponseMsg<String> upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new Exception("上传文件不能为空");
        } else {
            // 得到文件流
            final InputStream is = file.getInputStream();
            // 文件名
            final String fileName = file.getOriginalFilename();
            // 把文件放到minio的boots桶里面
            minioClient.putObject(minioData.getBucketName(), fileName, is, new PutObjectOptions(is.available(), -1));
            // 关闭输入流
            is.close();
            return ResponseMsg.buildSuccessMsg("上传成功");
        }
    }

    @ApiOperation(value = "删除文件")
    @GetMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SneakyThrows(Exception.class)
    public ResponseMsg<String> delete(@RequestParam("fileName") String fileName) {
        minioClient.removeObject(minioData.getBucketName(), fileName);
        return ResponseMsg.buildSuccessMsg("删除成功");
    }
}
