package com.dzx.test.sharding.demo;

import com.dzx.test.sharding.demo.dao.PositionRepository;
import com.dzx.test.sharding.demo.entity.Position;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = DemoApplication.class)
class DemoApplicationTests {


    @Autowired
    private PositionRepository positionRepository;


    @Test
    void contextLoads() {
        for (int i = 1; i <= 20; i++) {
            Position position = new Position();
            position.setId((long) i);
            position.setName("lagou" + i);
            position.setSalary("1000000");
            position.setCity("beijing");
            positionRepository.save(position);
        }
    }

}
